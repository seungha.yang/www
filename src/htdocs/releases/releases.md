# GStreamer Releases

- **Latest stable release:** [1.18.5][latest-stable]
- **Latest development release:** [1.19.3][1.19.3], [git main][latest-devel]

[latest-stable]: https://gstreamer.freedesktop.org/releases/1.18/#1.18.5
[latest-devel]: https://gitlab.freedesktop.org/gstreamer/gstreamer/
[1.19.3]: https://lists.freedesktop.org/archives/gstreamer-devel/2021-November/079212.html

### Current release

The current API and ABI stable GStreamer 1.x series is:

- [GStreamer 1.18 release series](https://gstreamer.freedesktop.org/releases/1.18/)

### Older releases

These releases are all part of the API and ABI stable GStreamer 1.x series:

- [GStreamer 1.16 release series](https://gstreamer.freedesktop.org/releases/1.16/)
- [GStreamer 1.14 release series](https://gstreamer.freedesktop.org/releases/1.14/)
- [GStreamer 1.12 release series](https://gstreamer.freedesktop.org/releases/1.12/)
- [GStreamer 1.10 release series](https://gstreamer.freedesktop.org/releases/1.10/)
- [GStreamer 1.8 release series](https://gstreamer.freedesktop.org/releases/1.8/)
- [GStreamer 1.6 release series](https://gstreamer.freedesktop.org/releases/1.6/)
- GStreamer 1.4 release series
- GStreamer 1.2 release series
- GStreamer 1.2 release series

### Prehistoric releases

- GStreamer 0.10 was the previous API series (2005-2013) and is no longer
  maintained or supported.

- - -

*License: [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/)*
